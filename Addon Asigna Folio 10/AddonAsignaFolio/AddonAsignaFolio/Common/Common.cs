﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddonAsignaFolio.Common
{
    public class Common
    {
        public bool AsinFolio(int DocEntry, string SerieName, int TipoDoc)
        {
            string Query = null, Query2 = null;
            SAPbobsCOM.Recordset oRecordset = null;
            SAPbobsCOM.Documents oDocuments = null;
            SAPbobsCOM.StockTransfer oStockTransfer = null;
            int Error = 0;
            string FolioPref = null, NextFolio = null, DefESeries = null, ObjectCode = null;
            int iNextFolio = -1;
            try
            {
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = @"SELECT FolioPref, NextFolio,Remark,DefESeries,ObjectCode FROM NNM1 WHERE Remark = 'ASFL' AND Series ='" + SerieName + "'";
                switch (TipoDoc)
                {
                    case 13://Factura
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                        break;
                    case 34:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                        oDocuments.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_InvoiceExempt;
                        break;
                    case 61:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
                        break;
                    case 56:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                        oDocuments.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_DebitMemo;
                        break;
                    case 15: //Entrega
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                        break;
                    case 16://Devolucion en ventas
                        //oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oReturns);
                        break;
                    case 67://Transferencia de Stock
                        oStockTransfer = (SAPbobsCOM.StockTransfer)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer);
                        break;
                    case 18://Factura de Proveedores
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);
                        break;
                    case 21://Devolucion de mercacia en compras 
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseReturns);
                        break;

                }
                oRecordset.DoQuery(Query);
                FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                NextFolio = Convert.ToString(oRecordset.Fields.Item(1).Value);
                DefESeries = Convert.ToString(oRecordset.Fields.Item(3).Value);
                ObjectCode = Convert.ToString(oRecordset.Fields.Item(4).Value);
                switch (TipoDoc)
                {
                    case 67:
                        oStockTransfer.GetByKey(DocEntry);
                        oStockTransfer.FolioPrefixString = FolioPref;
                        oStockTransfer.FolioNumber = int.Parse(NextFolio);
                        //oStockTransfer.Printed = SAPbobsCOM.PrintStatusEnum.psYes;
                        Error = oStockTransfer.Update();
                        break;
                    default:
                        oDocuments.GetByKey(DocEntry);
                        oDocuments.FolioPrefixString = FolioPref;
                        oDocuments.FolioNumber = int.Parse(NextFolio);
                        oDocuments.Printed = SAPbobsCOM.PrintStatusEnum.psYes;
                        Error = oDocuments.Update();
                        break;
                }

                iNextFolio = int.Parse(NextFolio);
                iNextFolio = 1 + iNextFolio;
                
                if (Error.Equals(0))
                {
                    oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    switch (ObjectCode)
                    {
                        
                        case "67":
                        case "21":
                            Query = @" UPDATE 
                               NNM1 
                               SET
                               NextFolio = '" + iNextFolio + @"'                            
                               WHERE
                               Remark = 'ASFL'
                               AND ObjectCode IN (15,67,21) 
                               AND ( DefESeries = '" + DefESeries + @"' OR Series = '" + DefESeries + @"' ) 
                           ";
                            oRecordset.DoQuery(Query);                            
                            break;
                        case "15":
                            Query = @" UPDATE 
                               NNM1 
                               SET
                               NextFolio = '" + iNextFolio + @"'                            
                               WHERE
                               Remark = 'ASFL'
                               AND ObjectCode IN (15,67,21) 
                               AND ( DefESeries = '" + SerieName + @"' OR Series = '" + SerieName + @"' ) 
                           ";
                            oRecordset.DoQuery(Query);
                            break;
                    }
                    
                    /*
                    Query = @" UPDATE
                                     NNM1
                                SET
                                     NNM1.NextFolio = '" + iNextFolio + @"'
                                FROM
                                    NNM1,
	                                NNM1 T1     
                                WHERE 
                                T1.Remark = 'ASFL' 
                                AND NNM1.Series = '" + SerieName + @"'
                                AND T1.DefESeries != '0'
                                OR NNM1.DefESeries = T1.Series 
                                OR NNM1.Series = T1.DefESeries ";
                                */
                    //Query = "UPDATE NNM1 SET [NextFolio]= '" + iNextFolio + "'  WHERE [Series] = '" + SerieName + "' OR DefESeries = '" + SerieName + "'";
                    


                    Query2 = "UPDATE NNM1 SET [NextFolio]= '" + iNextFolio + "'  WHERE [Series] = '" + SerieName + "'";
                    oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    oRecordset.DoQuery(Query2);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                GC.Collect();
            }
        }
    }
}
