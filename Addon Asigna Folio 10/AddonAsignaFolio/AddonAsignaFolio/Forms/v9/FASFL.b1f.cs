﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace AddonAsignaFolio91.Forms
{
    [FormAttribute("AddonAsignaFolio91.Forms.FASFL", "Forms/FASFL.b1f")]
    class FASFL : UserFormBase
    {
        public FASFL()
        {
            if (!string.IsNullOrEmpty(Menu.ColId))
            {
                EditText2.Value = Menu.ColId;
                SearchDefFol(EditText2.Value);
            }
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("stFolioP").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("etFolioP").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("stNextF").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("etNextF").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("etColId").Specific));
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("cbSeries").Specific));
            this.ComboBox1.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox1_ComboSelectAfter);
            this.OnCustomInitialize();
        }

        private void ComboBox1_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            string FolioPref = null;
            string NextFolio = null;
            //string Remark = null, SerieName = null;
            //string DefESeries = null;
            try
            {
                string Combo = ComboBox1.Selected.Description;
                string pattern = @"-";
                String[] elements = System.Text.RegularExpressions.Regex.Split(Combo, pattern);
                FolioPref = elements[0].ToString();
                NextFolio = elements[1].ToString();

                this.UIAPIRawForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                EditText0.Value = FolioPref.Trim();
                EditText1.Value = NextFolio.Trim();
                

            }
            catch
            {
            }
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {

        }
        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            string Query = null, Query2 = null;
            string FolioPref = null;
            string NextFolio = null;
            string Remark = null, SerieName = null, ObjectCode = null;
            string DefESeries = null;
            try
            {
                FolioPref = EditText0.Value;
                NextFolio = EditText1.Value;
                Remark = "ASFL";
                SerieName = EditText2.Value;
                DefESeries = ComboBox1.Value.Trim(); 
                Query = "UPDATE NNM1 SET [FolioPref] = '" + FolioPref + "' , [NextFolio]= '" + NextFolio + "' , [Remark] = '" + Remark + "' , [DefESeries] = '" + DefESeries + "'   WHERE [Series] = '" + SerieName + "'";
                SAPbobsCOM.Recordset oRecordset = null;
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery(Query);

                Query = @"SELECT FolioPref, NextFolio,Remark,DefESeries,ObjectCode FROM NNM1 WHERE Remark = 'ASFL' AND Series ='" + SerieName + "'";
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery(Query);
                FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                NextFolio = Convert.ToString(oRecordset.Fields.Item(1).Value);
                DefESeries = Convert.ToString(oRecordset.Fields.Item(3).Value);
                ObjectCode = Convert.ToString(oRecordset.Fields.Item(4).Value);

                switch (ObjectCode)
                {

                    case "67":
                    case "21":
                        Query2 = @" UPDATE 
                               NNM1 
                               SET
                               NextFolio = '" + NextFolio + @"'                            
                               WHERE
                               Remark = 'ASFL'
                               AND ObjectCode IN (15,67,21) 
                               AND ( DefESeries = '" + DefESeries + @"' OR Series = '" + DefESeries + @"' ) 
                           ";                       
                        break;
                    case "15":
                        Query2 = @" UPDATE 
                               NNM1 
                               SET
                               NextFolio = '" + NextFolio + @"'                            
                               WHERE
                               Remark = 'ASFL'
                               AND ObjectCode IN (15,67,21) 
                               AND ( DefESeries = '" + SerieName + @"' OR Series = '" + SerieName + @"' ) 
                           ";
                        
                        break;
                }   
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery(Query2);
                this.UIAPIRawForm.Close();
                Application.SBO_Application.MessageBox("Se asigno Folio Correctamente!", 1, "OK");


                /*
                 Query2 = @"UPDATE 
                                 NNM1 
                             SET 
                                 NNM1.FolioPref = '" + FolioPref + @"' , 
                                 NNM1.NextFolio = '" + NextFolio + @"'                            
                             FROM
                                 NNM1,
                                 NNM1 T1
                         WHERE
                             T1.Remark = 'ASFL'
                             AND NNM1.Series = '" + SerieName + @"'                        
                             OR NNM1.DefESeries = T1.Series
                             OR NNM1.Series = T1.DefESeries";
                             */
            }
            catch (Exception )
            {

            }
            finally
            {
                GC.Collect();
            }
        }
        
        private void SearchDefFol(string Series)
        {
            SAPbobsCOM.Recordset oRecordset = null;
            SAPbobsCOM.Recordset oRecordset2 = null;
            try
            {
                oRecordset2 = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset2.DoQuery(" SELECT Series, CONVERT(VARCHAR(50), FolioPref) +' - ' + CONVERT(VARCHAR(50), NextFolio)  AS NextFolio FROM NNM1 WHERE Remark = 'ASFL'  AND DefESeries = '' AND Series != '" + Series + "' ");
                for(int i = 0;i < oRecordset2.RecordCount; i++ )
                {
                    ComboBox1.ValidValues.Add(Convert.ToString(oRecordset2.Fields.Item(0).Value), Convert.ToString(oRecordset2.Fields.Item(1).Value));
                    oRecordset2.MoveNext();
                }
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("SELECT T0.[FolioPref], T0.[NextFolio], T0.[Remark],T0.[DefESeries] FROM NNM1 T0 WHERE T0.[Series] = '" + Series + "'");
                string FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                string NextFolio = Convert.ToString(oRecordset.Fields.Item(1).Value);
                string Remark = Convert.ToString(oRecordset.Fields.Item(2).Value);
                string DefESeries = Convert.ToString(oRecordset.Fields.Item(3).Value);
                if (!string.IsNullOrEmpty(FolioPref))
                {
                    EditText0.Value = FolioPref;
                    EditText1.Value = NextFolio;
                    if ( !string.IsNullOrEmpty(DefESeries) && !DefESeries.Equals("0"))
                        ComboBox1.Select(DefESeries,SAPbouiCOM.BoSearchKey.psk_ByValue);
                    this.UIAPIRawForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }
                else
                {
                    this.UIAPIRawForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                }

            }
            catch (Exception)
            {

            }
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.ComboBox ComboBox1;
        private SAPbouiCOM.Button Button1;
    }
}
