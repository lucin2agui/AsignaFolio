﻿using System;
using System.Collections.Generic;
using SAPbouiCOM.Framework;
using AddonAsignaFolio.Forms;

namespace AddonAsignaFolio
{
    class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    oApp = new Application(args[0]);
                }
                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();
                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);
                Application.SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler(SBO_Application_AppEvent);
                SBO.oCompany = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
                crearEstruturaAddon();
                Application.SBO_Application.FormDataEvent += new SAPbouiCOM._IApplicationEvents_FormDataEventEventHandler(SBO_Appl_FormDataEvent);
                Application.SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler(SBO_Appl_ItemEvent);
                Application.SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler(SBO_Appl_MenuEvent);

                oApp.Run();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        #region Events
        static void SBO_Application_AppEvent(SAPbouiCOM.BoAppEventTypes EventType)
        {
            switch (EventType)
            {
                case SAPbouiCOM.BoAppEventTypes.aet_ShutDown:
                    //Exit Add-On
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_FontChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged:
                    System.Windows.Forms.Application.Exit();
                    break;
                case SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition:
                    System.Windows.Forms.Application.Exit();
                    break;
                default:
                    break;
            }
        }

        static void SBO_Appl_FormDataEvent(ref SAPbouiCOM.BusinessObjectInfo BusinessObjectInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                SAPbouiCOM.Form oForm = null;
                oForm = Application.SBO_Application.Forms.Item(BusinessObjectInfo.FormUID);
                switch (BusinessObjectInfo.FormTypeEx)
                {
                    #region Factura
                    case "133":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _133 objCss = new _133();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Facturas

                    #region Nota de Credito de Clientes
                    case "179":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _179 objCss = new _179();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Nota de Credito de Clientes

                    #region Factura Execta deudores
                    case "65302":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _65302 objCss = new _65302();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Factura Execta deudores

                    #region Nota Debito Clientes
                    case "65303":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _65303 objCss = new _65303();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Nota Debito Clientes

                    #region Entrega Venta
                    case "140":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _140 objCss = new _140();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Entrega Venta

                    #region Devolucion - Venta
                    case "180":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            //_180 objCss = new _180();
                            //objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Devolucion - Venta

                    #region Transferencia de Stock
                    case "940":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _940 objCss = new _940();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Transferencia de Stock

                    #region Factura de Proveedores
                    case "141":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _141 objCss = new _141();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                    #endregion Factura de Proveedores

                    #region Devolución de mercancías - Compras
                    case "182":
                        if (BusinessObjectInfo.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD && BusinessObjectInfo.BeforeAction == false)
                        {
                            _182 objCss = new _182();
                            objCss.Form_DataAddAfter(ref BusinessObjectInfo, oForm);
                        }
                        break;
                        #endregion Devolución de mercancías - Compras
                }

            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox("SBO_Appl_FormDataEvent " + ex.Message);
            }
            finally
            {

            }
        }

        static void SBO_Appl_ItemEvent(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                switch (pVal.FormTypeEx)
                {
                    case "SEI_INT":
                        //SEI_FormIntegracion.m_SBO_Appl_ItemEvent(FormUID, ref pVal, out BubbleEvent);
                        break;
                }
            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox("SBO_Appl_ItemEvent " + ex.Message);
            }
            finally
            {
                GC.Collect();
            }
        }

        static void SBO_Appl_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (pVal.BeforeAction)
                {
                    switch (pVal.MenuUID)
                    {
                        case "SEI_INT":
                            //SEI_FormIntegracion oProv = new SEI_FormIntegracion("SEI_INT");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox("SBO_Appl_MenuEvent " + ex.Message);
            }
            finally
            {

            }

        }

        static void crearEstruturaAddon()
        {
            AddonAsignaFolio.Common.Security sec = new AddonAsignaFolio.Common.Security();
            sec.CrearTablaUsuario("SECURITY", "Security", SAPbobsCOM.BoUTBTableType.bott_NoObject, Application.SBO_Application);
            sec.crearCampo("SECURITY", "KEY", "KEY", SAPbobsCOM.BoFieldTypes.db_Memo, 0, SAPbobsCOM.BoFldSubTypes.st_None, "", "", false, null);
            sec.crearCampo("SECURITY", "ValidTo", "Valid to", SAPbobsCOM.BoFieldTypes.db_Date, 0, SAPbobsCOM.BoFldSubTypes.st_None, "", "", false, null);
            sec.RegistraAddon();
            //sec.CrearCampoUsuario("SECURITY", "KEY", "KEY", SAPbobsCOM.BoFieldTypes.db_Memo, 0, Application.SBO_Application);
            //sec.CrearCampoUsuario("SECURITY", "ValidTo", "Valid to", SAPbobsCOM.BoFieldTypes.db_Date, 0, Application.SBO_Application);

        }

        #endregion Events
    }
}
