﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace AddonAsignaFolio.Forms
{
    [FormAttribute("AddonAsignaFolio.Forms.FormLicAddon", "Forms/FormLicAddon.b1f")]
    class FormLicAddon : UserFormBase
    {
        public FormLicAddon()
        {
        }

       

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("lbRuta").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("tbPathFile").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("btSearch").Specific));
            this.Button0.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button0_ClickBefore);
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("btLoad").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private void OnCustomInitialize()
        {

        }

        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        /// <summary>
        /// Explora archivos en el equipo
        /// </summary>
        /// <param name="sboObject"></param>
        /// <param name="pVal"></param>
        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            try
            {
                SAPbouiCOM.Form oForm = Application.SBO_Application.Forms.Item(this.UIAPIRawForm.UniqueID);
                AddonAsignaFolio.Common.FileManager fm = new Common.FileManager();
                fm.OpenFile(oForm, "tbPathFile");
            }
            catch //(Exception ex)
            {

            }
        }

        /// <summary>
        /// Agrega licencia del addon en tabla 
        /// </summary>
        /// <param name="sboObject"></param>
        /// <param name="pVal"></param>
        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbouiCOM.Form oForm = Application.SBO_Application.Forms.Item(this.UIAPIRawForm.UniqueID);
            Common.FileManager fm = new Common.FileManager();
            fm.UploadLicense(this.EditText0.Value);
        }

        private void Button0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
        }

        #region Open dialogo

        public static void DoWork()
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Working thread...");
                System.Threading.Thread.Sleep(100);
            }
        }
       /* private string showOpenFileDialog()
        {
            string path = System.Windows.Forms.Application.StartupPath;
            System.Threading.Thread ShowFolderBrowserThread;
            try
            {
                ShowFolderBrowserThread = new System.Threading.Thread(DoWork);
                if (ShowFolderBrowserThread.ThreadState == System.Threading.ThreadState.Unstarted)
                {
                    ShowFolderBrowserThread.SetApartmentState(System.Threading.ApartmentState.STA);
                    ShowFolderBrowserThread.Start();
                }
                else if (ShowFolderBrowserThread.ThreadState == System.Threading.ThreadState.Stopped)
                {
                    ShowFolderBrowserThread.Start();
                    ShowFolderBrowserThread.Join();
                }

                while (ShowFolderBrowserThread.ThreadState == System.Threading.ThreadState.Running)
                {
                    System.Windows.Forms.Application.DoEvents();
                }
                if (FileName <> "")
                {
                    return FileName;
                }
            }
            catch (Exception ex)
            {
                SBO_Application.MessageBox("FileFile" + ex.Message);
                MessageBox.Show(ex.ToString())
                   return "";
            }
        }
        */



        #endregion Open dialogo
    }
}
