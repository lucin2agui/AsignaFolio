﻿using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM.Framework;
using AddonAsignaFolio.Forms;

namespace AddonAsignaFolio
{
    class Menu
    {
        public static string ColId;
        public void AddMenuItems()
        {
            SAPbouiCOM.Menus oMenus = null;
            SAPbouiCOM.MenuItem oMenuItem = null;

            oMenus = Application.SBO_Application.Menus;

            SAPbouiCOM.MenuCreationParams oCreationPackage = null;
            oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuItem = Application.SBO_Application.Menus.Item("3328"); // moudles'

            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
            oCreationPackage.UniqueID = "AsignaFolio";
            oCreationPackage.String = "Asigna Folio";
            oCreationPackage.Enabled = true;
            //oCreationPackage.Position = -1;
            oMenus = oMenuItem.SubMenus;
            try
            {
                //  If the manu already exists this code will fail
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception)
            {

            }
            try
            {
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "AddonAsignaFolio.Forms.FormLicAddon";
                oCreationPackage.String = "Licencia Addon";
                oMenus.AddEx(oCreationPackage);
            }
            catch { }
        }

        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction)
                {
                    SAPbouiCOM.MenuItem oMenuItem = null;
                    SAPbouiCOM.Menus oMenus = null;
                    AddonAsignaFolio.Common.Security sec = new AddonAsignaFolio.Common.Security();
                   
                        switch (pVal.MenuUID)
                        {
                            case "8203":
                                break;
                            case "ASFL":
                            if (sec.ValidLic()) //Licencia Valida
                            {
                                FASFL oForm = new FASFL();
                                oForm.Show();
                            }
                            else Application.SBO_Application.MessageBox("Debe asignar una licencia valida.\n Favor comuniquese con su proveedor.!");
                            break;
                            default:
                                oMenuItem = Application.SBO_Application.Menus.Item("1280");// 'Data'
                                oMenus = (SAPbouiCOM.Menus)oMenuItem.SubMenus;
                                if (oMenus.Exists("ASFL"))
                                {
                                    Application.SBO_Application.Menus.RemoveEx("ASFL");
                                    //Application.SBO_Application.Menus.RemoveEx("ACFL");
                                }
                                break;
                        }
                    if ((pVal.MenuUID == "AddonAsignaFolio.Forms.FormLicAddon"))
                    {
                        AddonAsignaFolio.Forms.FormLicAddon formLic = new AddonAsignaFolio.Forms.FormLicAddon();
                        formLic.Show();
                    }
                }
                                  
            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox(ex.ToString(), 1, "Ok", "", "");
            }
        }

    }
}
