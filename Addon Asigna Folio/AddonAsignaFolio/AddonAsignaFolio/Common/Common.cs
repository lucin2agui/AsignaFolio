﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddonAsignaFolio.Common
{
    public class Common
    {
        public bool AsinFolio(int DocEntry, string SerieName,int TipoDoc)
        {
            string Query = null;
            SAPbobsCOM.Recordset oRecordset = null;
            SAPbobsCOM.Documents oDocuments = null;
            string FolioPref = null, NextFolio = null;
            int iNextFolio = 0;
            try
            {                
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                Query = @"SELECT FolioPref, NextFolio,Remark FROM NNM1 WHERE Remark = 'ASFL' AND Series ='" + SerieName + "'";
                switch (TipoDoc)
                {
                    case 13:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                        break;
                    case 34:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                        oDocuments.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_InvoiceExempt;
                        break;
                    case 61:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
                        break;
                    case 56:
                        oDocuments = (SAPbobsCOM.Documents)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
                        oDocuments.DocumentSubType = SAPbobsCOM.BoDocumentSubType.bod_DebitMemo;
                        break;
                }
                oRecordset.DoQuery(Query);
                FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                NextFolio = Convert.ToString(oRecordset.Fields.Item(1).Value);

                oDocuments.GetByKey(DocEntry);
                oDocuments.FolioPrefixString = FolioPref;
                oDocuments.FolioNumber = int.Parse(NextFolio);
                oDocuments.Printed = SAPbobsCOM.PrintStatusEnum.psYes;
                iNextFolio = int.Parse(NextFolio);
                iNextFolio = 1 + iNextFolio;
                int Error = oDocuments.Update();
                if (Error.Equals(0))
                {
                    oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    Query = "UPDATE NNM1 SET [NextFolio]= '" + iNextFolio + "'  WHERE [Series] = '" + SerieName + "'";
                    oRecordset.DoQuery(Query);
                    return true;
                }
                else
                {
                    return false;
                }
                GC.Collect();      

            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
