﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace AddonAsignaFolio.Forms
{
    [FormAttribute("185", "Forms/185.b1f")]
    class _185 : SystemFormBase
    {
        public _185()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("4").Specific));
            this.Matrix0.ClickAfter += new SAPbouiCOM._IMatrixEvents_ClickAfterEventHandler(this.Matrix0_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.RightClickAfter += new RightClickAfterHandler(this.Form_RightClickAfter);
            this.RightClickBefore += new RightClickBeforeHandler(this.Form_RightClickBefore);

        }

        private void Form_RightClickAfter(ref SAPbouiCOM.ContextMenuInfo eventInfo)
        {
            //throw new System.NotImplementedException();

        }

        private void OnCustomInitialize()
        {

        }

        private void Form_RightClickBefore(ref SAPbouiCOM.ContextMenuInfo eventInfo, out bool BubbleEvent)
        {
            BubbleEvent = true;
            //throw new System.NotImplementedException();
            AddMenu(ref eventInfo);

        }

        private void AddMenu(ref SAPbouiCOM.ContextMenuInfo eventInfo)
        {
            try
            {
                //if (eventInfo.FormUID == "RightClk")
                //{
                if (eventInfo.BeforeAction)
                {
                    SAPbouiCOM.MenuItem oMenuItem = null;
                    SAPbouiCOM.Menus oMenus = null;
                    try
                    {
                        
                        if (!Application.SBO_Application.Menus.Exists("1280"))
                        {
                            oMenuItem = Application.SBO_Application.Menus.Item("1280");// 'Data'
                            oMenus = (SAPbouiCOM.Menus)oMenuItem.SubMenus;
                            //Menu.ColId = eventInfo.Row.ToString();
                        }
                        else {
                            oMenuItem = Application.SBO_Application.Menus.Item("1280");// 'Data'
                            oMenus = (SAPbouiCOM.Menus)oMenuItem.SubMenus;

                            if(oMenus.Exists("ASFL"))
                            {
                                var series = ((SAPbouiCOM.EditText)Matrix0.Columns.Item("10").Cells.Item(eventInfo.Row).Specific).String;
                                Menu.ColId = series;
                            }
                            else
                            {
                                var series = ((SAPbouiCOM.EditText)Matrix0.Columns.Item("10").Cells.Item(eventInfo.Row).Specific).String;
                                Menu.ColId = series;

                                SAPbouiCOM.MenuCreationParams oCreationPackage = null;
                                oCreationPackage = (SAPbouiCOM.MenuCreationParams)Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                                oCreationPackage.UniqueID = "ASFL";
                                oCreationPackage.String = "Asignar Folio";
                                oCreationPackage.Enabled = true;


                                oMenuItem = Application.SBO_Application.Menus.Item("1280");// 'Data'
                                oMenus = (SAPbouiCOM.Menus)oMenuItem.SubMenus;
                                oMenus.AddEx(oCreationPackage);

                                //oCreationPackage = (SAPbouiCOM.MenuCreationParams)Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams);
                                //oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                                //oCreationPackage.UniqueID = "ACFL";
                                //oCreationPackage.String = "Actualizar Folio";
                                //oCreationPackage.Enabled = true;

                                //oMenuItem = Application.SBO_Application.Menus.Item("1280");// 'Data'
                                //oMenus = (SAPbouiCOM.Menus)oMenuItem.SubMenus;
                                //oMenus.AddEx(oCreationPackage);
                            }
                        }

                        
                    }

                    catch (Exception ex)
                    {
                        Application.SBO_Application.MessageBox(ex.Message);
                    }
                }
                else
                {
                    SAPbouiCOM.MenuItem oMenuItem = null;
                    SAPbouiCOM.Menus oMenus = null;
                    try
                    {
                        Application.SBO_Application.Menus.RemoveEx("ASFL");
                        //Application.SBO_Application.Menus.RemoveEx("ACFL");
                    }
                    catch (Exception ex)
                    {
                        Application.SBO_Application.MessageBox(ex.Message);
                    }
                }
                //}
            }
            catch
            {

            }

        }

        private SAPbouiCOM.Matrix Matrix0;

        private void Matrix0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            

        }

        
    }
}
