﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace AddonAsignaFolio.Forms
{
    [FormAttribute("65302", "Forms/65302.b1f")]
    class _65302 : SystemFormBase
    {
        public _65302()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataAddAfter += new DataAddAfterHandler(this.Form_DataAddAfter);

        }

        private void Form_DataAddAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            string sDocEntry = null, Series = null, Query = null, FolioPref = null, Canceled = null;
            try
            {
                Canceled = (this.UIAPIRawForm.DataSources.DBDataSources.Item("OINV").GetValue("CANCELED", 0).ToString());
                if (!pVal.BeforeAction && pVal.ActionSuccess && Canceled != "Y")
                {
                    SAPbobsCOM.Recordset oRecordset = null;
                    oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    sDocEntry = (this.UIAPIRawForm.DataSources.DBDataSources.Item("OINV").GetValue("DocEntry", 0).ToString());
                    Series = (this.UIAPIRawForm.DataSources.DBDataSources.Item("OINV").GetValue("Series", 0).ToString());
                    Query = @"SELECT FolioPref, NextFolio,Remark FROM NNM1 WHERE Remark = 'ASFL' AND Series ='" + Series + "'";
                    oRecordset.DoQuery(Query);
                    if (!oRecordset.EoF)
                    {
                        FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                        if (!string.IsNullOrEmpty(FolioPref))
                        {
                            Common.Common com = new Common.Common();
                            com.AsinFolio(int.Parse(sDocEntry), Series, 34);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void OnCustomInitialize()
        {

        }
    }
}
