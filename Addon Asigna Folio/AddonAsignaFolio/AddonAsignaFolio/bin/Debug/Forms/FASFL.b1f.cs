﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace AddonAsignaFolio.Forms
{
    [FormAttribute("AddonAsignaFolio.Forms.FASFL", "Forms/FASFL.b1f")]
    public class FASFL : UserFormBase
    {
        public FASFL()
        {
            if(!string.IsNullOrEmpty(Menu.ColId))
            {                
                EditText2.Value = Menu.ColId;
                SearchDefFol(EditText2.Value);
            }
        }
        
       /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("stFolioP").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("etFolioP").Specific));
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("stNextF").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("etNextF").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("etColId").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }        

        private void OnCustomInitialize()
        {

        }       

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            string Query = null;
            string FolioPref = null;
            string NextFolio =null;
            string Remark = null,SerieName =null;
            try
            {
                FolioPref = EditText0.Value;
                NextFolio = EditText1.Value;
                Remark = "ASFL";
                SerieName = EditText2.Value;

                Query = "UPDATE NNM1 SET [FolioPref] = '" + FolioPref + "' , [NextFolio]= '" + NextFolio + "' , [Remark] = '" + Remark + "'   WHERE [Series] = '" + SerieName + "'";

                SAPbobsCOM.Recordset oRecordset = null;
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery(Query);
                this.UIAPIRawForm.Close();
                Application.SBO_Application.MessageBox("Se asigno Folio Correctamente!", 1, "OK");
            }
            catch (Exception ex)
            {

            }
        }

        private void SearchDefFol(string Series)
        {
            SAPbobsCOM.Recordset oRecordset = null;
            try
            {
                oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("SELECT T0.[FolioPref], T0.[NextFolio], T0.[Remark] FROM NNM1 T0 WHERE T0.[Series] = '" + Series + "'");
                string FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                string NextFolio = Convert.ToString(oRecordset.Fields.Item(1).Value);
                string Remark = Convert.ToString(oRecordset.Fields.Item(2).Value);
                if (!string.IsNullOrEmpty(FolioPref))
                {
                    EditText0.Value = FolioPref;
                    EditText1.Value = NextFolio;
                    this.UIAPIRawForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE;
                }
                else
                {
                    this.UIAPIRawForm.Mode = SAPbouiCOM.BoFormMode.fm_ADD_MODE;
                }

            }
            catch (Exception ex)
            {

            }
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.StaticText StaticText0;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.Button Button1;
    }
}
