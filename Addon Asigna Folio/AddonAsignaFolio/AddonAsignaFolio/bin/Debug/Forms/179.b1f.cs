﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace AddonAsignaFolio.Forms
{
    [FormAttribute("179", "Forms/179.b1f")]
    class _179 : SystemFormBase
    {
        public _179()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataAddAfter += new DataAddAfterHandler(this.Form_DataAddAfter);

        }

        private void Form_DataAddAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            string sDocEntry = null, Series = null, Query = null, FolioPref = null;
            try
            {
                if (!pVal.BeforeAction && pVal.ActionSuccess)
                {
                    SAPbobsCOM.Recordset oRecordset = null;
                    oRecordset = (SAPbobsCOM.Recordset)SBO.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    sDocEntry = (this.UIAPIRawForm.DataSources.DBDataSources.Item("ORIN").GetValue("DocEntry", 0).ToString());
                    Series = (this.UIAPIRawForm.DataSources.DBDataSources.Item("ORIN").GetValue("Series", 0).ToString());
                    Query = @"SELECT FolioPref, NextFolio,Remark FROM NNM1 WHERE Remark = 'ASFL' AND Series ='" + Series + "'";
                    oRecordset.DoQuery(Query);
                    if (!oRecordset.EoF)
                    {
                        FolioPref = Convert.ToString(oRecordset.Fields.Item(0).Value);
                        if (!string.IsNullOrEmpty(FolioPref))
                        {
                            Common.Common com = new Common.Common();
                            com.AsinFolio(int.Parse(sDocEntry), Series, 61);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        private void OnCustomInitialize()
        {

        }
    }
}
